terraform {
  required_version = ">= 0.15"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.39.0"
    }

    null = {
      source  = "hashicorp/null"
      version = "3.1.0"
    }
  }
}