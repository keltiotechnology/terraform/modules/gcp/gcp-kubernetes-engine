output "ca_certificate" {
  description = "Cluster ca certificate (base64 encoded)"
  value       = module.gke-cluster.ca_certificate
  sensitive   = true
}

output "cluster_id" {
  description = "Cluster ID"
  value       = module.gke-cluster.cluster_id
}

output "endpoint" {
  description = "Cluster endpoint"
  value       = module.gke-cluster.endpoint
}

output "instance_group_urls" {
  description = "List of GKE generated instance groups"
  value       = module.gke-cluster.instance_group_urls
}

output "location" {
  description = "Cluster location (region if regional cluster, zone if zonal cluster)"
  value       = module.gke-cluster.location
}

output "master_version" {
  description = "Current master kubernetes version"
  value       = module.gke-cluster.master_version
}

output "name" {
  description = "Cluster name"
  value       = module.gke-cluster.name
}

output "network_policy_enabled" {
  description = "Whether network policy enabled"
  value       = module.gke-cluster.network_policy_enabled
}

output "service_account" {
  description = "The service account to default running nodes"
  value       = module.gke-cluster.service_account
}

output "type" {
  description = "Cluster type (regional / zonal)"
  value       = module.gke-cluster.type
}