## GCP Project ##
gcp_project        = "keltio-app-1584542324136"
gcp_default_region = "europe-west1"
gke_cluster_settings = {
  name                       = "gke-cluster-test"
  region                     = "europe-west1"
  zones                      = ["europe-west1-b"]
  network                    = "gke-cluster-net"
  subnetwork                 = "cluster-subnet"
  ip_range_pods              = "pods-ip-range"
  ip_range_services          = "kubesvc-ip-range"
  http_load_balancing        = false
  horizontal_pod_autoscaling = true
  network_policy             = false
  enable_private_endpoint    = false
  enable_private_nodes       = true
  master_ipv4_cidr_block     = "10.0.0.0/28"
}

gke_node_pools = [
  {
    name               = "default-node-pool"
    machine_type       = "e2-standard-2"
    autoscaling        = false
    node_count         = 1
    local_ssd_count    = 0
    disk_size_gb       = 100
    disk_type          = "pd-standard"
    image_type         = "COS"
    auto_repair        = true
    auto_upgrade       = true
    preemptible        = false
    initial_node_count = 1
    tags               = "testing"
  },
]