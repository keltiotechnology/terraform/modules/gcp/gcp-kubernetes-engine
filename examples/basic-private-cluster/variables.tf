## GCP Project ##
variable "gcp_project" {
  type        = string
  description = "The Google Cloud Project ID"
}

variable "gcp_default_region" {
  type        = string
  description = "The default GCP region to deploy the cluster"
}

## Google Kubernetes Engine Private Cluster ##
variable "gke_cluster_settings" {
  type        = any
  description = "The GKE Cluster settings"
  default = {
    name                       = "gke-test-1"
    region                     = "us-central1"
    zones                      = ["us-central1-a", "us-central1-b", "us-central1-f"]
    network                    = "vpc-01"
    subnetwork                 = "us-central1-01"
    ip_range_pods              = "us-central1-01-gke-01-pods"
    ip_range_services          = "us-central1-01-gke-01-services"
    http_load_balancing        = false
    horizontal_pod_autoscaling = true
    network_policy             = false
    enable_private_endpoint    = true
    enable_private_nodes       = true
    master_ipv4_cidr_block     = "10.0.0.0/28"
  }
}

variable "gke_node_pools" {
  type        = list(map(string))
  description = "List of maps containing node pools configs"
  default = [
    {
      name               = "default-node-pool"
      machine_type       = "e2-standard-2"
      autoscaling        = false
      node_count         = 3
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 3
      tags               = "testing"
    },
  ]
}