## Network Settings ##
variable "network_name" {
  type        = string
  description = "The name of the Compute network"
}

variable "network_routing_mode" {
  type        = string
  description = "The network routing mode"
  default     = "GLOBAL"
}

variable "network_subnets" {
  type        = list(map(string))
  description = "The list of subnets being created"
}

variable "secondary_ranges" {
  type        = map(list(object({ range_name = string, ip_cidr_range = string })))
  description = "Secondary ranges that will be used in some of the subnets"
}

variable "network_routes" {
  type        = list(map(string))
  description = "The list of optional routing rules"
  default = [
    {
      name              = "egress-internet"
      description       = "route through IGW to access internet"
      destination_range = "0.0.0.0/0"
      tags              = "egress-inet"
      next_hop_internet = "true"
    },
  ]
}