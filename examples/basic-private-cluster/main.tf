provider "google" {
  project = var.gcp_project
  region  = var.gcp_default_region
}

## Network and subnets settings ##
module "gcp-network" {
  source  = "terraform-google-modules/network/google"
  version = "~> 3.0"

  project_id       = var.gcp_project
  network_name     = var.network_name
  routing_mode     = var.network_routing_mode
  subnets          = var.network_subnets
  secondary_ranges = var.secondary_ranges
  routes           = var.network_routes
}

resource "google_compute_router" "router" {
  name    = "${var.gke_cluster_settings.name}-router"
  region  = var.gke_cluster_settings.region
  network = var.gke_cluster_settings.network

  bgp {
    asn = 64514
  }
}

resource "google_compute_router_nat" "nat" {
  name                               = "${var.gke_cluster_settings.name}-nat"
  router                             = "${var.gke_cluster_settings.name}-router"
  region                             = var.gke_cluster_settings.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

module "gke-cluster" {
  source = "../.."

  gcp_project          = var.gcp_project
  gcp_default_region   = var.gcp_default_region
  gke_cluster_settings = var.gke_cluster_settings
  gke_node_pools       = var.gke_node_pools
}