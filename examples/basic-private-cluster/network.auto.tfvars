network_name         = "gke-cluster-net"
network_routing_mode = "GLOBAL"
network_subnets = [
  {
    subnet_name   = "cluster-subnet"
    subnet_ip     = "10.1.0.0/16"
    subnet_region = "europe-west1"
  }
]
secondary_ranges = {
  "cluster-subnet" = [
    {
      ip_cidr_range = "172.1.0.0/16"
      range_name    = "pods-ip-range"
    },
    {
      ip_cidr_range = "172.2.0.0/16"
      range_name    = "kubesvc-ip-range"
    }
  ]
}
network_routes = [
  {
    name              = "egress-internet"
    description       = "route through IGW to access internet"
    destination_range = "0.0.0.0/0"
    tags              = "egress-inet"
    next_hop_internet = "true"
  },
]