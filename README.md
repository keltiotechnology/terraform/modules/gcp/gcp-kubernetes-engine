<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Google Kubernetes Engine

Terraform module which create user defined GKE cluster

## Module usage

```hcl
provider "google" {
  project = var.gcp_project
  region  = var.gcp_default_region
}

## Network and subnets settings ##
module "gcp-network" {
  source  = "terraform-google-modules/network/google"
  version = "~> 3.0"

  project_id       = var.gcp_project
  network_name     = var.network_name
  routing_mode     = var.network_routing_mode
  subnets          = var.network_subnets
  secondary_ranges = var.secondary_ranges
  routes           = var.network_routes
}

resource "google_compute_router" "router" {
  name    = "${var.gke_cluster_settings.name}-router"
  region  = var.gke_cluster_settings.region
  network = var.gke_cluster_settings.network

  bgp {
    asn = 64514
  }
}

resource "google_compute_router_nat" "nat" {
  name                               = "${var.gke_cluster_settings.name}-nat"
  router                             = "${var.gke_cluster_settings.name}-router"
  region                             = var.gke_cluster_settings.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

module "gke-cluster" {
  source = "../.."

  gcp_project          = var.gcp_project
  gcp_default_region   = var.gcp_default_region
  gke_cluster_settings = var.gke_cluster_settings
  gke_node_pools       = var.gke_node_pools
}
```
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 3.39.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | 3.1.0 |

## Resources

| Name | Type |
|------|------|
| [google_compute_firewall.allow-ingress-validating-webhooks](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [null_resource.get-kubeconfig](https://registry.terraform.io/providers/hashicorp/null/3.1.0/docs/resources/resource) | resource |
| [google_client_config.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 3.39.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_default_region"></a> [gcp\_default\_region](#input\_gcp\_default\_region) | The default GCP region to deploy the cluster | `string` | n/a | yes |
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | The Google Cloud Project ID | `string` | n/a | yes |
| <a name="input_gke_cluster_settings"></a> [gke\_cluster\_settings](#input\_gke\_cluster\_settings) | The GKE Cluster settings | `any` | <pre>{<br>  "enable_private_endpoint": true,<br>  "enable_private_nodes": true,<br>  "horizontal_pod_autoscaling": true,<br>  "http_load_balancing": false,<br>  "ip_range_pods": "us-central1-01-gke-01-pods",<br>  "ip_range_services": "us-central1-01-gke-01-services",<br>  "master_ipv4_cidr_block": "10.0.0.0/28",<br>  "name": "gke-test-1",<br>  "network": "vpc-01",<br>  "network_policy": false,<br>  "region": "us-central1",<br>  "subnetwork": "us-central1-01",<br>  "zones": [<br>    "us-central1-a",<br>    "us-central1-b",<br>    "us-central1-f"<br>  ]<br>}</pre> | no |
| <a name="input_gke_node_pools"></a> [gke\_node\_pools](#input\_gke\_node\_pools) | List of maps containing node pools configs | `list(map(string))` | <pre>[<br>  {<br>    "auto_repair": true,<br>    "auto_upgrade": true,<br>    "autoscaling": false,<br>    "disk_size_gb": 100,<br>    "disk_type": "pd-standard",<br>    "image_type": "COS",<br>    "initial_node_count": 3,<br>    "local_ssd_count": 0,<br>    "machine_type": "e2-standard-2",<br>    "name": "default-node-pool",<br>    "node_count": 3,<br>    "preemptible": false,<br>    "tags": "testing"<br>  }<br>]</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ca_certificate"></a> [ca\_certificate](#output\_ca\_certificate) | Cluster ca certificate (base64 encoded) |
| <a name="output_cluster_id"></a> [cluster\_id](#output\_cluster\_id) | Cluster ID |
| <a name="output_endpoint"></a> [endpoint](#output\_endpoint) | Cluster endpoint |
| <a name="output_instance_group_urls"></a> [instance\_group\_urls](#output\_instance\_group\_urls) | List of GKE generated instance groups |
| <a name="output_location"></a> [location](#output\_location) | Cluster location (region if regional cluster, zone if zonal cluster) |
| <a name="output_master_version"></a> [master\_version](#output\_master\_version) | Current master kubernetes version |
| <a name="output_name"></a> [name](#output\_name) | Cluster name |
| <a name="output_network_policy_enabled"></a> [network\_policy\_enabled](#output\_network\_policy\_enabled) | Whether network policy enabled |
| <a name="output_service_account"></a> [service\_account](#output\_service\_account) | The service account to default running nodes |
| <a name="output_type"></a> [type](#output\_type) | Cluster type (regional / zonal) |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->