data "google_client_config" "default" {}

# This provider block is needed by the gke-cluster module #
provider "kubernetes" {
  host                   = "https://${module.gke-cluster.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke-cluster.ca_certificate)
}

module "gke-cluster" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version = "17.2.0"

  project_id                 = var.gcp_project
  name                       = var.gke_cluster_settings.name
  region                     = var.gke_cluster_settings.region
  zones                      = var.gke_cluster_settings.zones
  network                    = var.gke_cluster_settings.network
  subnetwork                 = var.gke_cluster_settings.subnetwork
  ip_range_pods              = var.gke_cluster_settings.ip_range_pods
  ip_range_services          = var.gke_cluster_settings.ip_range_services
  http_load_balancing        = var.gke_cluster_settings.http_load_balancing
  horizontal_pod_autoscaling = var.gke_cluster_settings.horizontal_pod_autoscaling
  network_policy             = var.gke_cluster_settings.network_policy
  enable_private_endpoint    = var.gke_cluster_settings.enable_private_endpoint
  enable_private_nodes       = var.gke_cluster_settings.enable_private_nodes
  master_ipv4_cidr_block     = var.gke_cluster_settings.master_ipv4_cidr_block
  node_pools                 = var.gke_node_pools
  node_pools_oauth_scopes = {
    "all" : [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring"
    ]
  }
}

resource "google_compute_firewall" "allow-ingress-validating-webhooks" {
  name    = "${var.gke_cluster_settings.name}-firewall"
  network = var.gke_cluster_settings.network

  allow {
    protocol = "tcp"
    ports    = ["443", "10250", "10254", "8443"]
  }
}

resource "null_resource" "get-kubeconfig" {
  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${var.gke_cluster_settings.name} --region ${var.gke_cluster_settings.region} --project ${var.gcp_project}"
  }

  depends_on = [module.gke-cluster]
}